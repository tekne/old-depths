use amethyst::{
    core::transform::TransformBundle,
    prelude::*,
    renderer::{
        plugins::{RenderPbr3D, RenderToWindow},
        types::DefaultBackend,
        RenderingBundle,
    },
    input::{InputBundle, StringBindings},
    controls::{MouseFocusUpdateSystemDesc, CursorHideSystem},
    utils::application_root_dir,
};

mod depths;
use depths::{PlayerKeys, Depths};
mod physics;
use physics::PhysicsSystemDesc;

fn main() -> amethyst::Result<()> {
    amethyst::start_logger(Default::default());

    let app_root = application_root_dir()?;

    let assets_dir = app_root.join("assets");
    let config_dir = app_root.join("config");
    let display_config_path = config_dir.join("display.ron");
    let binding_path = config_dir.join("bindings.ron");

    let rendering_bundle = RenderingBundle::<DefaultBackend>::new()
        .with_plugin(
            RenderToWindow::from_config_path(display_config_path)?
                .with_clear([0.0, 0.0, 0.0, 1.0]),
        )
        .with_plugin(RenderPbr3D::default());

    let input_bundle = InputBundle::<StringBindings>::new()
        .with_bindings_from_file(binding_path)?;

    let game_data = GameDataBuilder::default()
        .with_bundle(rendering_bundle)?
        .with_bundle(input_bundle)?
        .with_bundle(TransformBundle::new())?
        .with_system_desc(MouseFocusUpdateSystemDesc, "mouse_focus_update_system", &[])
        .with(CursorHideSystem::new(), "cursor_hide_system", &["mouse_focus_update_system"])
        .with_system_desc(PhysicsSystemDesc, "physics_system", &[])
        .with(PlayerKeys, "player_keys", &["input_system"]);

    let mut game = Application::new(assets_dir, Depths, game_data)?;
    game.run();

    Ok(())
}
