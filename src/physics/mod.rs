use amethyst::{
    core::{
        math,
        transform::Transform,
        timing::Time
    },
    ecs::{System, SystemData, ReadStorage, WriteStorage, Read, WriteExpect, join::Join},
    ecs::prelude::{Component, DenseVecStorage},
    derive::SystemDesc
};
use nphysics3d::{
    object::{
        DefaultBodySet, DefaultColliderSet, BodyStatus, DefaultBodyHandle, DefaultBodyPartHandle,
        BodyPartHandle, RigidBodyDesc
    },
    force_generator::DefaultForceGeneratorSet,
    joint::DefaultJointConstraintSet,
    world::{DefaultMechanicalWorld, DefaultGeometricalWorld}
};
use nalgebra::{Unit, Vector3};
use std::ops::DerefMut;

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Anchor(pub DefaultBodyPartHandle);

impl Anchor {
    pub fn rigid(body: DefaultBodyHandle) -> Anchor { Anchor(BodyPartHandle(body, 0)) }
    pub fn body(&self) -> DefaultBodyHandle { (self.0).0 }
    pub fn part_id(&self) -> usize { (self.0).1 }
    pub fn part(&self) -> DefaultBodyPartHandle { self.0 }
}

impl Component for Anchor {
    type Storage = DenseVecStorage<Self>;
}

pub type Mechanics = DefaultMechanicalWorld<f64>;
pub type Geometry = DefaultGeometricalWorld<f64>;
pub type Bodies = DefaultBodySet<f64>;
pub type Colliders = DefaultColliderSet<f64>;
pub type Joints = DefaultJointConstraintSet<f64>;
pub type Forces = DefaultForceGeneratorSet<f64>;

#[derive(Default, SystemDesc)]
#[system_desc(insert("Mechanics::new(Vector3::new(0.0, 0.0, 0.0))"))]
#[system_desc(insert("Geometry::new()"))]
#[system_desc(insert("Bodies::new()"))]
#[system_desc(insert("Joints::new()"))]
#[system_desc(insert("Colliders::new()"))]
#[system_desc(insert("Forces::new()"))]
#[system_desc(name(PhysicsSystemDesc))]
pub struct PhysicsSystem;

pub type WritePhysics<'a> = (
    WriteExpect<'a, Mechanics>,
    WriteExpect<'a, Geometry>,
    WriteExpect<'a, Bodies>,
    WriteExpect<'a, Colliders>,
    WriteExpect<'a, Joints>,
    WriteExpect<'a, Forces>
);

impl<'a> System<'a> for PhysicsSystem {
    type SystemData = (
        WriteStorage<'a, Transform>,
        ReadStorage<'a, Anchor>,
        WritePhysics<'a>,
        Read<'a, Time>
    );

    fn run(&mut self, (mut transforms, anchors, physics, time): Self::SystemData) {
        let (mut mechanics, mut geometry, mut bodies, mut colliders, mut joints, mut forces)
            = physics;
        let dt = time.delta_seconds();
        mechanics.set_timestep(dt as f64);
        mechanics.step(
            geometry.deref_mut(),
            bodies.deref_mut(),
            colliders.deref_mut(),
            joints.deref_mut(),
            forces.deref_mut());
        for (transform, anchor) in (&mut transforms, &anchors).join() {
            let body = if let Some(body) = bodies.get(anchor.body()) { body } else { continue };
            let part = if let Some(part) = body.part(anchor.part_id()) { part } else { continue };
            let pos = part.position();
            let translation = pos.translation.vector;
            let rotation = pos.rotation.quaternion().coords;
            transform.set_translation_xyz(
                translation[0] as f32,
                translation[1] as f32,
                translation[2] as f32
            );
            transform.set_rotation(
                math::Unit::new_unchecked(math::Quaternion {
                    coords: math::Vector4::from([
                        rotation[0] as f32,
                        rotation[1] as f32,
                        rotation[2] as f32,
                        rotation[3] as f32
                    ])
                })
            );
        }
    }
}
