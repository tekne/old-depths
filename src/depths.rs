use amethyst::{
    assets::{AssetLoaderSystemData},
    core::{
        transform::Transform,
        timing::Time
    },
    ecs::{System, SystemData, World, WriteStorage, Read, WriteExpect, join::Join},
    ecs::prelude::{Component, DenseVecStorage},
    derive::SystemDesc,
    input::{
        is_key_down, StringBindings, VirtualKeyCode, InputHandler
    },
    prelude::*,
    renderer::{
        Camera, Mesh, Material, MaterialDefaults,
        shape::Shape,
        rendy::mesh::{Normal, Position, TexCoord, Tangent},
        light::{Light, PointLight},
        palette::rgb::Rgb
    },
};
use nphysics3d::{
    object::{ColliderDesc, RigidBodyDesc},
    material::{MaterialHandle, BasicMaterial},
    math::Force,
    algebra::ForceType
};
use nalgebra::{Vector3, Isometry3};
use ncollide3d::{
    shape::{ShapeHandle, Ball}
};
use super::physics::{Bodies, Colliders, Anchor};

pub struct Depths;

impl SimpleState for Depths {
    fn on_start(&mut self, data: StateData<'_, GameData<'_, '_>>) {
        initialize_player(data.world);
        initialize_spheres(data.world);
        initialize_light(data.world);
    }

    fn handle_event(&mut self, _: StateData<'_, GameData<'_, '_>>, event: StateEvent)
    -> SimpleTrans {
        if let StateEvent::Window(event) = event {
            if is_key_down(&event, VirtualKeyCode::Escape) {
                Trans::Quit
            } else {
                Trans::None
            }
        } else { Trans::None }
    }

}

#[derive(Debug, Copy, Clone, PartialEq, Default)]
pub struct PlayerControlled;

impl Component for PlayerControlled {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, SystemDesc)]
pub struct PlayerKeys;

impl<'a> System<'a> for PlayerKeys {
    type SystemData = (
        WriteStorage<'a, PlayerControlled>,
        WriteStorage<'a, Transform>,
        WriteStorage<'a, Anchor>,
        WriteExpect<'a, Bodies>,
        Read<'a, InputHandler<StringBindings>>
    );

    fn run(&mut self,
        (mut players, mut transforms, mut anchors, mut bodies, input): Self::SystemData) {
        for (_player, anchor, transform) in (&mut players, &mut anchors, &mut transforms).join() {
            if let Some((x, y)) = input.mouse_position() {
                // Centering
                let cx = 2048.0/2.0 - x;
                let cy = 1024.0/2.0 - y;
                // Normalizing
                let px = cx / 2048.0;
                let py = cy / 1024.0;
                // Set x rotation
                transform.set_rotation_x_axis(py * std::f32::consts::PI);
                transform.append_rotation_y_axis(px * std::f32::consts::PI);
            }
            let mut f = Vector3::zeros();
            f[0] += input.axis_value("move_x").unwrap_or(0.0) as f64;
            f[1] += input.axis_value("move_y").unwrap_or(0.0) as f64;
            f[2] += input.axis_value("move_z").unwrap_or(0.0) as f64;
            if let Some(body) = bodies.get_mut(anchor.body()) {
                body.apply_force(
                    anchor.part_id(),
                    &Force { linear: 0.05 * f, angular: Vector3::zeros() },
                    ForceType::VelocityChange,
                    true
                )
            }
        }
    }
}

fn initialize_player(world: &mut World) {
    let light: Light = PointLight {
        intensity: 2.0,
        color: Rgb::new(1.0, 1.0, 1.0),
        ..PointLight::default()
    }.into();

    let mut transform = Transform::default();
    transform.set_translation_xyz(0.0, 0.0, 10.0);

    let rigid_body = world.exec(|mut bodies: WriteExpect<'_, Bodies>| {
        let desc = RigidBodyDesc::new()
            .translation(nalgebra::Vector3::z() * 10.0)
            .build();
        let body = bodies.insert(desc);
        Anchor::rigid(body)
    });

    world.create_entity()
        .with(Camera::standard_3d(2048.0, 1024.0))
        .with(PlayerControlled)
        .with(transform)
        .with(rigid_body)
        .with(light)
        .build();
}

fn initialize_spheres(world: &mut World) {
    let mut transform = Transform::default();
    transform.set_translation_xyz(0.0, 0.0, 0.0);

    let mesh = world.exec(|loader: AssetLoaderSystemData<'_, Mesh>| {
        loader.load_from_data(
            Shape::Sphere(100, 100)
            .generate::<(Vec<Position>, Vec<Normal>, Vec<Tangent>, Vec<TexCoord>)>(None)
            .into(), ()
        )
    });
    let material_defaults = world.read_resource::<MaterialDefaults>().0.clone();
    let material = world.exec(|loader: AssetLoaderSystemData<'_, Material>| {
        loader.load_from_data(
            Material { ..material_defaults }, ()
        )
    });

    let anchor = world.exec(|mut bodies: WriteExpect<'_, Bodies>| {
        let desc = RigidBodyDesc::new()
            .build();
        let body = bodies.insert(desc);
        Anchor::rigid(body)
    });

    let _collider = world.exec(|mut colliders: WriteExpect<'_, Colliders>| {
        let shape = ShapeHandle::new(Ball::new(5.0));
        let collider = ColliderDesc::new(shape).build(anchor.part());
        colliders.insert(collider)
    });

    world.create_entity()
        .with(mesh.clone())
        .with(material.clone())
        .with(transform.clone())
        .with(anchor)
        .build();

    transform.set_translation_xyz(3.0, 0.0, 0.0);

    world.create_entity()
        .with(mesh)
        .with(material)
        .with(transform)
        .build();
}

fn initialize_light(world: &mut World) {
    let light: Light = PointLight {
        intensity: 6.0,
        color: Rgb::new(0.3, 0.2, 0.0),
        ..PointLight::default()
    }.into();

    let mut transform = Transform::default();
    transform.set_translation_xyz(5.0, 5.0, 20.0);

    world
        .create_entity()
        .with(light)
        .with(transform)
        .build();
}
